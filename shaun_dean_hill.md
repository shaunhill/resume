
Shaun Dean Hill
=========================

 >11 years experience in the IT industry with 7+ years experience in data related design, implementation, and support.
Specializing in data processing, profiling, and integration. With strong design, modeling and visualization experience. I
have a diverse understanding of a variety of technologies while maintaining an in-depth knowledge of technologies in
which I specialize. This gives me the perspective to take into account pre-existing, current and potential technology investments, and
integrate these considerations into a solution.

Technologies
--------------------

   >Java, Python, Pandas , ABAP, T-SQL, MS SSIS, MS SSRS, SAP BW, SAP BEx Query Designer, SAP Design Studio, SAP Web Intelligence, SAP Crystal Reports, R, Dplyr GGplot2, D3.js, HTML, CSS, Javascript, Twitter Bootstrap, JQuery, MongoDB, MS SQL Server, SAP AAOE, SAP ECC, Windows Server, VMware ESX, Github, Tableau, Bloomberg Terminal, Bloomberg API, Micro-Services, MS Power BI

Corporate Experience
--------------------

2017-10 - Present : *BI Developer, [Digital Outsource Services](https://www.digioutsource.com/) (Cape Town, South Africa)*

>As part of the team responsible for data processing, and reporting on a large affiliate marketing system, we value data integrity and availability most.  Our time is invested towards not only innovating but also understanding and quantifying the bottom line impact of our action. This means that not only do function as developers but also subject matter experts and data analysts. We mostly leverage Microsoft stack with a heavy focus towards batch based TSQL data processing, but recently we have begun to experiment with next technologies and paradigms to ensure we are ready for the future.
> Some projects I assisted on during this time:
>
- Creating a framework, to allow the team to create data checks which will automatically prevent potential bad data from being displayed in operational reporting.  
- Working together with the team to completely rewrite the data processing and reporting system, with our focus being improving scalability, availability, stability, and to lay the groundwork for event-based processing. This is not only an exercise in optimization and sustainability. But also challenges us to think in a new paradigm.
- Rewriting our affiliation code to allow for the ability to process legacy tracking information, and to reduce complexity and technical debt.

---
2017-04 - 2017-09 : *Data Analyst, [Bloomberg LP](https://www.bloomberg.com/company/) (Cape Town, South Africa)*

  >Providing technical and data expertise to leverage the Bloomberg technology stack to derive insights, identify data issue, and drive related projects. Conduct technical skills training and provide guidance to colleagues. Use financial markets and accounting knowledge to create a consensus on listed EMEA companies ahead of their financial reporting across various core ratios, income statement, balance sheet and cash flow measures.

---

2016-11 - 2017-03 : *BI Developer/Consultant, Freelance [shaundh.co.za](http://shaunhill.github.io/) (Gauteng, South Africa)*

  >Requirements gathering, design and, implementation of business intelligence solutions which are both functional and practical. In the ever changing world of data, I seek to gain knowledge and apply what I have learned to help others to do the same.
>
- Utilizing Python, MongoDB, to create an event based data processing system to consume and process Twitter streams, with the intention of training a model to automatically identify discrimination.  [Procject-rot](https://github.com/shaunhill/project-rot)

---


<div style="page-break-after: always;"></div>




2016-06 - 2016-10 : *BI Technical Consultant, [Zetta Solutions (Pty)](http://www.zettasolutions.com/) (Fourways, Gauteng, South Africa)*

>Worked closely with senior management and staff to adhere to tight deadlines and weekly deliverables. Maintaining and
enhancing existing developments, while implementing new developments to ensuring project progress, information availability and fast turnaround times.
Utilizing various technologies implementing a wide range of solutions including:
>
- Outlining a data strategy, to lay the groundwork for future developments.
- Designing and developing a Microsoft SQL 2014 database to accommodate the front-end and the various reports and dashboards.
- Using tools like T-SQL, SQL Server Integration Services, and, excel to consolidate and standardize both historical data and master data
- Using ASP.net Dynamic Data to create a multi-user web based data capture system.
- Designing, developing and refactoring several Tableau 10 dashboard and reports, this included integration with the clients WMS (Web Map Service) and [shapefiles](http://doc.arcgis.com/en/arcgis-online/reference/shapefiles.htm).

---


2014-08 - 2016-07 : *BI Developer/Consultant, Freelance [shaundh.co.za](http://shaunhill.github.io/) (Gauteng, South Africa)*

>Professionally I worked on various SAP BW/BO project, but my time was mainly focused on upskilling and diversifying my skill set. I worked with various technologies including [R (Programming language)](https://en.wikipedia.org/wiki/R_(programming_language)), [MongoDB (NoSQL Database)](https://en.wikipedia.org/wiki/MongoDB)],and [d3.js (Low Level Visualization library)](https://en.wikipedia.org/wiki/D3.js).
Some examples of projects worked on during this time:
>
- Creating a SAP Design Studio dashboard for mining quantities and equipment KPIs
- Various SAP BW projects which included working with procurement, financial, mining, GPS, and equipment related data, as well enhancing standard and custom developments.
- Extracting and analyzing openstreetmap.org data with the intention of correlated the data with data extracted from company websites to identify missing stores/branches, and then adding the delta to openstreetmap.org.
- Creating a Java based web crawler search websites for company contact information.

---

2012-08 - 2014-07 : *BI Developer, [MCC Group](https://www.mccgroup.co.za) (Midrand, Gauteng, South Africa)*

 >Being involved in all aspects of the enterprise data warehouse model, I gained a wide range of BI and data related skills. Successfully designing, implementing and supporting both Microsoft SQL and SAP BW data warehouses, As well as reporting and analytical developments in SAP Business Objects, Qlikview, and SSRS. I also assisted in other data related tasks, e.g. using LSMW and BAPIs to do data take on, and master data generation.

---

  2010-08 - 2012-07 : *Database / Systems Administrator, [MCC Group](https://www.mccgroup.co.za)  (Midrand, Gauteng, South Africa).*

 >Responsible for the administration and maintenance of servers both physical and virtual, network infrastructure, IT related procurement and supplier management. It was during this time that I gained my initial exposure to databases and T-SQL. As my knowledge grew I took over the responsibility for databases and applications, and ultimately business intelligence.

---

  2007-08 - 2010-07 : *Network Administrator, [Eqstra Holdings](https://www.eqstra.co.za) (Meadowdale, Johannesburg, South Africa).*


---




<div style="page-break-after: always;"></div>


Core Skills
--------------------

*Design / Modeling / Planning*

>Strong knowledge of database(relational and non-Relational) and variety of data models, and processing paradigm. I have worked on implementation and design teams, doing requirements gathering, gap analyses, development, consultancy, post implementation, support, and training.


*Data Processing / Integration  / ETL*

  >A large portion of my experience is focused towards data processing, with experience designing and maintaining OLTP, OLAP, and Event based systems. I have work with a variety of data sources including : Relational, non-relation, flat-file, web APIs and unstructured date.



*Development / Programming*

>Familiarity with various design patterns and programming paradigms and have mainly worked with Python and Java to develop a few data related systems. Although most of my programming experience is data originated, I have also developed, front-ends, websites, and as mentioned below visualizations.  

*Visualization / Reporting*

>Visualization and reporting are a major part of my skill sets. Most recently I worked with Tableau and SAP Business Objects. Although some time ago, I also made extensive use of Microsoft SSRS and Qlikview. In my personal capacity, I work with R-GGplot and D3.js.


*Data Analysis / Profiling*

  > Experience in using T-SQL, R, Tableau, Python, Excel, and other Excel related add-ons like SAP AAOE for analyses and profiling. I am also familiar with the core concepts of descriptive and inferential statistics, as well as machine learning.

Contact Details
--------------------

   >***Email*** : shaun4z@live.co.za |  ***Cell*** : +27 (72) 535 9760 |  ***Linkedin*** : <https://za.linkedin.com/in/shaundh>
